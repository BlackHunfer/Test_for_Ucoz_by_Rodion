import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import './style.css'
import './assets/fonts/font-awesome/css/font-awesome.min.css'
import VueSmoothScroll from 'vue-smooth-scroll'
import VueClipboard from 'vue-clipboard2'
import VueSlideToggle from 'vue-slide-toggle'


import Task1 from './Task1.vue'
import Task2 from './Task2.vue'
import Task3 from './Task3.vue'
import Task4 from './Task4.vue'
import Task5 from './Task5.vue'
import Task6 from './Task6.vue'


Vue.use(VueSmoothScroll)
Vue.use(VueClipboard)
Vue.use(VueSlideToggle)
Vue.use(ElementUI)

Vue.component ('task1', Task1)
Vue.component ('task2', Task2)
Vue.component ('task3', Task3)
Vue.component ('task4', Task4)
Vue.component ('task5', Task5)
Vue.component ('task6', Task6)

const app = new Vue({
  render: h => h(App),
  data:function(){
      return {
        checked: true
      }
  }
}).$mount('#app');

